No need to add any configuration for this library to work. Remember to add the path to the library in the @ComponentScan annotation on the application class, e.g.:

@ComponentScan(basePackages = {
"com.jbdev.httputils.*",
"your_project_path"
})

Features:
 - Logs incoming and outgoing HTTP requests/responses 