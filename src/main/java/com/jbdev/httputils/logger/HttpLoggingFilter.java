package com.jbdev.httputils.logger;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

@Slf4j
@Component
public class HttpLoggingFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        ContentCachingRequestWrapper requestWrapper = new ContentCachingRequestWrapper(request);
        ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper(response);
        filterChain.doFilter(requestWrapper, responseWrapper);
        logResponse(requestWrapper, responseWrapper);
    }

    private void logResponse(ContentCachingRequestWrapper requestWrapper, ContentCachingResponseWrapper responseWrapper) throws IOException {

        log.info("-------   -------   ------- INCOMING HTTP REQUEST   -------   -------   -------");
        log.info(requestWrapper.getMethod() + requestWrapper.getRequestURI());

        List<String> requestHeaders = new ArrayList<>();
        Enumeration<String> headerNames = requestWrapper.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            String headerValue = requestWrapper.getHeader(headerName);
            requestHeaders.add(headerName + " " + headerValue);
        }

        log.info("Headers: " + requestHeaders);
        log.info("Request body: {}", new String(requestWrapper.getContentAsByteArray()));

        log.info("-------   -------   ------- OUTGOING HTTP RESPONSE   -------   -------   -------");
        log.info("Response status code: " + responseWrapper.getStatus());
        log.info("Response body: {}", new String(responseWrapper.getContentAsByteArray()));
        responseWrapper.copyBodyToResponse();
        log.info("-------   -------   -------    -------   -------   -------");

    }
}
